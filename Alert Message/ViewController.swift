//
//  ViewController.swift
//  Alert Message
//
//  Created by Paul Pearson on 2/18/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    
    @IBAction func showAlertClicked(sender: AnyObject) {
        let alertController = UIAlertController(title: "iOScreator", message:
            "Hello, world!", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

